import traceback
import requests
import json
import sys
import time
import argparse

API_TYPE_LISK_V0_9 = "lisk_v0.9"
API_TYPE_ARK_V2 = "ark_v2"
API_TYPE_SOLAR = "solar"
SUPPORTED_API_TYPES = [API_TYPE_LISK_V0_9, API_TYPE_ARK_V2, API_TYPE_SOLAR]

if sys.version_info[0] < 3:
    print('python2 not supported, please use python3')
    sys.exit()

parser = argparse.ArgumentParser(description='Automatic payment script')
parser.add_argument(
    '-t', '--test',
    action='store_true',
    help='Use this flag to run the script in test mode, no transactions will be made',
    required=False
)
parser.add_argument(
    '-a', '--address',
    help='Override the wallet address from which to make the transaction',
    required=False
)
parser.add_argument(
    '-s1', '--secret1',
    help='Override the primary secret',
    required=False
)
parser.add_argument(
    '-s2', '--secret2',
    help='Override the secondary secret',
    required=False
)
parser.add_argument(
    '--payout',
    help='Override the payout configuration as a JSON string',
    required=False
)
args = parser.parse_args()


def handle_error(ex, msg, exit):
    error_msg = time.strftime('%Y-%m-%d %H:%M:%S: ') + msg
    if ex is not None:
        error_msg += ': ' + str(ex) + '\n\n'
        error_msg += traceback.format_exc().rstrip() + '\n'
    print(error_msg)
    if exit:
        sys.exit()


def to_coin(amount):
    return int(amount * 100000000)


def from_coin(amount):
    return amount / 100000000


def send_transactions_solar(node_url, transactions, secret, second_secret):
    import transactions_solar

    networks = networks_ark['Networks']
    network_id = conf['network']

    network_info = next(
        (network_info for network_info in networks if network_info['id'] == network_id),
        None
    )
    if not network_info:
        handle_error(None, 'unknown network: ' + network_id, True)

    transactions_solar.build_network(
        node_url + '/api',
        network_info['epoch'],
        network_info['version'],
        network_info['wif']
    )
    signed_transactions = None
    mapped_transactions = []
    for transaction in transactions:
        mapped_transactions.append(
            transactions_solar.Transaction(
                transaction['address'],
                int(transaction['amount'])
            )
        )
        signed_transactions = transactions_solar.build_transfer_transaction(
            mapped_transactions,
            to_coin(conf['transaction_fee']),
            secret,
            second_secret
        )

    if not signed_transactions or len(signed_transactions) == 0:
        handle_error(None, 'no transactions to send', True)

    success, error = transactions_solar.broadcast(signed_transactions)
    if success is False:
        handle_error(None, 'Failed to send transaction: ' + error, True)


def send_transactions_ark_v2(node_url, transactions, secret, second_secret):
    import transactions_ark_v2

    networks = networks_ark['Networks']
    network_id = conf['network']

    network_info = next(
        (network_info for network_info in networks if network_info['id'] == network_id),
        None
    )
    if not network_info:
        handle_error(None, 'unknown network: ' + network_id, True)

    transactions_ark_v2.build_network(
        node_url + '/api',
        network_info['epoch'],
        network_info['version'],
        network_info['wif']
    )
    signed_transactions = None
    if len(transactions) == 1:
        transaction = transactions[0]
        signed_transactions = transactions_ark_v2.build_transfer_transaction(
            transactions_ark_v2.Transaction(
                transaction['address'],
                int(transaction['amount'])
            ),
            to_coin(conf['transaction_fee']),
            secret,
            second_secret
        )
    elif len(transactions) > 1:
        mapped_transactions = []
        for transaction in transactions:
            mapped_transactions.append(
                transactions_ark_v2.Transaction(
                    transaction['address'],
                    int(transaction['amount'])
                )
            )
            signed_transactions = transactions_ark_v2.build_multi_transaction(
                mapped_transactions,
                to_coin(conf['transaction_fee']),
                secret,
                second_secret
            )

    if not signed_transactions or len(signed_transactions) == 0:
        handle_error(None, 'no transactions to send', True)

    success, error = transactions_ark_v2.broadcast(signed_transactions)
    if success is False:
        handle_error(None, 'Failed to send transaction: ' + error, True)


def send_transactions_lisk_v0_9(node_url, transactions, secret, second_secret):
    for transaction in transactions:
        address_to = transaction['address']
        amount = transaction['amount']

        try:
            request_url = node_url + '/api/transactions'
            if second_secret:
                body_json = {"amount": amount, "recipientId": address_to, "secret": secret,
                             "secondSecret": second_secret}
            else:
                body_json = {"amount": amount, "recipientId": address_to, "secret": secret}

            response = requests.put(request_url, json=body_json)
            if response.status_code == 200:
                response_json = response.json()
                if not response_json['success']:
                    handle_error(None, 'Failed to send transaction ' + response_json, False)
            else:
                handle_error(None, str(response.status_code) + ' Failed to send transaction', False)
        except Exception as ex:
            handle_error(ex, 'Unable to send transaction', False)


def send_transactions(api_type, node_url, transactions, secret, second_secret):
    if mode_test:
        for transaction in transactions:
            print('Test transaction of ' + str(from_coin(transaction['amount'])) + ' to ' + transaction['address'])
        return

    if api_type == API_TYPE_LISK_V0_9:
        return send_transactions_lisk_v0_9(node_url, transactions, secret, second_secret)
    elif api_type == API_TYPE_ARK_V2:
        return send_transactions_ark_v2(node_url, transactions, secret, second_secret)
    elif api_type == API_TYPE_SOLAR:
        return send_transactions_solar(node_url, transactions, secret, second_secret)


def get_node_url():
    node_url = conf['node_url']
    if not node_url.startswith('http'):
        handle_error(None, 'node_url needs to be in the format http://localhost:<port>', True)
    if node_url.endswith('/'):
        node_url = node_url[:-1]

    return node_url


def get_api_type():
    api_type = conf['api_type']
    if api_type not in SUPPORTED_API_TYPES:
        handle_error(None, 'api_type must be one of: ' + ' '.join(SUPPORTED_API_TYPES), True)

    return api_type


def get_balance_ark_v2(node_url, address_delegate):
    try:
        request_url = node_url + '/api/wallets/' + address_delegate
        response = requests.get(request_url)
        if response.status_code == 200:
            response_json = response.json()
            return int(response_json['data']['balance'])
        else:
            handle_error(None, str(response.status_code) + ' Failed to get balance: ', True)
    except Exception as ex:
        handle_error(ex, 'Unable to get balance', True)


def get_balance_lisk_v0_9(node_url, address_delegate):
    try:
        request_url = node_url + '/api/accounts/getBalance?address=' + address_delegate
        response = requests.get(request_url)
        if response.status_code == 200:
            response_json = response.json()
            if response_json['success']:
                return int(response_json['balance'])
            else:
                handle_error(None, 'Failed to get balance', True)
        else:
            handle_error(None, str(response.status_code) + ' Failed to get balance', True)
    except Exception as ex:
        handle_error(ex, 'Unable to get balance', True)


def get_balance(api_type, node_url, address_delegate):
    if api_type == API_TYPE_LISK_V0_9:
        return get_balance_lisk_v0_9(node_url, address_delegate)
    elif api_type == API_TYPE_ARK_V2 or api_type == API_TYPE_SOLAR:
        return get_balance_ark_v2(node_url, address_delegate)


def auto_pay():
    try:
        api_type = get_api_type()
        node_url = get_node_url()
        address_delegate = args.address if args.address else conf['address_delegate']
        try:
            payout = json.loads(args.payout) if args.payout else conf['payout']
        except json.JSONDecodeError as e:
            handle_error(e, "Invalid JSON provided for --payout argument", True)
            return
        balance_exclude = to_coin(conf['balance_exclude'])
        if balance_exclude < to_coin(0.0):
            handle_error(None, 'Minimum balance_exclude is 0.0', True)
        transaction_minimum = to_coin(conf['transaction_minimum'])
        if transaction_minimum < to_coin(0.1):
            handle_error(None, 'Minimum transaction_minimum is 0.1', True)
        transaction_fee = to_coin(conf['transaction_fee'])
        if transaction_fee < to_coin(0.0):
            handle_error(None, 'Minimum transaction_fee is 0.0', True)

        secret = args.secret1 if args.secret1 else conf['secret']
        second_secret = args.secret2 if args.secret1 or args.secret2 else conf['second_secret']
        if second_secret == "SECRET2" or second_secret == "":
            second_secret = None

        balance = get_balance(api_type, node_url, address_delegate)
        balance_remainder = balance - balance_exclude - transaction_fee
        if balance_remainder <= 0:
            handle_error(None, 'Not enough balance, balance:' + str(from_coin(balance)) + ' balance_exclude:' + str(
                from_coin(balance_exclude)) + ' transaction_fee:' + str(
                from_coin(transaction_fee)) + ' balance_remainder:' + str(from_coin(balance_remainder)), True)

        fixed_payout = [payout_entry for payout_entry in payout if "fixed" in payout_entry]
        share_payout = [payout_entry for payout_entry in payout if "share" in payout_entry]

        transactions = []
        for payout_entry in fixed_payout:
            address = payout_entry['address']
            amount = to_coin(payout_entry['fixed'])
            if amount == 0:
                # allow to temporarily disable a payout with a 0 setting
                continue
            amount_without_fee = amount - transaction_fee
            if amount_without_fee < transaction_minimum:
                handle_error(None, 'Ignoring transaction to ' + address + ', amount ' + str(
                    from_coin(amount)) + ' minus transaction_fee ' + str(
                    from_coin(transaction_fee)) + ' below the transaction_minimum', False)
            else:
                balance_remainder -= amount
                transactions.append({
                    "address": address,
                    "amount": amount
                })

        total_share = 0
        total_share_amount = 0
        if balance_remainder > transaction_minimum:
            for payout_entry in share_payout:
                address = payout_entry['address']
                share = payout_entry['share']
                if share == 0:
                    # allow to temporarily disable a payout with a 0 setting
                    continue
                amount = int(share * balance_remainder / 100)
                amount_without_fee = amount - transaction_fee
                if amount_without_fee < transaction_minimum:
                    handle_error(None, 'Ignoring transaction to ' + address + ', amount ' + str(
                        from_coin(amount)) + ' minus transaction_fee ' + str(
                        from_coin(transaction_fee)) + ' below the transaction_minimum', False)
                else:
                    total_share += payout_entry['share']
                    total_share_amount += amount
                    transactions.append({
                        "address": address,
                        "amount": amount
                    })
            balance_remainder -= total_share_amount

        if len(transactions) == 0:
            handle_error(None, 'No transactions to pay out', True)
        if total_share < 0 or total_share > 100:
            handle_error(None, 'Total payout share ' + str(total_share) + ' needs to be between 0-100', True)
        if balance_remainder < 0:
            handle_error(None, 'Not enough balance ' + str(from_coin(balance)) + ' to pay for ' + str(
                len(transactions)) + ' transactions', True)

        send_transactions(api_type, node_url, transactions, secret, second_secret)

    except Exception as ex:
        handle_error(ex, 'Simple pay failed with generic exception', True)


try:
    mode_test = args.test
    conf = json.load(open('config.json', 'r'))
    networks_ark = json.load(open('networks.json', 'r'))
    auto_pay()
except Exception as ex:
    handle_error(ex, 'Unable to load config file', True)
