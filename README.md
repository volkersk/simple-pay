# simple-pay
## Prerequisites
```
sudo apt install python3-pip
pip3 install requests
# only required for api_type ark_v2
pip3 install arkecosystem-crypto
pip3 install arkecosystem-client
# only required for api_type solar
pip3 install solar-crypto
pip3 install solar-client
```


## Installation
Clone the git repo:
```
git clone https://bitbucket.org/volkersk/simple-pay.git
```

Configure *config.json*:

* address_delegate: Used as the sender address for all transactions
* payout address: Used as the recipient address for the payout transaction
* payout share [opt]: Percentage (1-100) of the balance to send to the configured address
* payout fixed [opt]: Fixed amount to send to the configured address
* balance_exclude: The balance amount (0.0>) to exclude from the payout
* transaction_minimum: The minimum amount (0.1>) to accept a payment transaction
* transaction_fee: The amount (0.0>) to exclude per payment transaction
* api_type: The type of API. One of; *lisk_v0.9*, *ark_v2*, *solar*
* node_url: Location of the node. For example; *http://localhost:9305*, *http://127.0.0.1:9305*, *https://wallet.shiftnrg.org*, etc
* secret: First passphrase of your wallet. Must match the wallet of the *address_delegate*
* second_secret: Second passphrase of your wallet. *Optional*, leave an empty String to use no second secret


## Arguments
* --test: Simulate an automatic payment. No actual transactions will be made. *Optional*
* --address: Override the wallet address from which to make the transaction. *Optional*
* --secret1: Override the primary secret. *Optional*
* --secret2: Override the secondary secret. *Optional*
* --payout: Override the payout configuration as a JSON string. *Optional*


## Example crontab
Edit your crontab with the example below (command: crontab -e)

```
15 8 * * Sat cd ~/simple-pay && python3 ~/simple-pay/simplepay.py >> ~/simple-pay/logs/error.log 2>&1
```


## No history
If you want to add the secrets through command line arguments without keeping history, consider using: https://stackoverflow.com/a/8473153
