from datetime import datetime

from client import ArkClient
from client.exceptions import ArkHTTPException
from crypto.configuration.network import set_custom_network
from crypto.identity.address import address_from_passphrase
from crypto.transactions.builder.multi_payment import MultiPayment
from crypto.transactions.builder.transfer import Transfer

global client


class Transaction(object):
    def __init__(self, address: str, amount: int):
        self.address = address
        self.amount = amount


def build_network(api_base_url: str, epoch_str: str, version: int, wif: int):
    global client
    client = ArkClient(api_base_url)

    e = epoch_str.split(",")
    t = [int(i) for i in e]
    epoch = datetime(t[0], t[1], t[2], t[3], t[4], t[5])
    set_custom_network(epoch, version, wif)


def broadcast(tx):
    transaction = client.transactions.create([tx])
    success = len(transaction['data']['accept']) != 0
    error = next(iter(transaction['errors'].values()), None) if 'errors' in transaction else None
    error_message = "" if error is None else error['message']
    return success, error_message


def build_transfer_transaction(tx: Transaction, tx_fee: int, pp: str, sp: str = None):
    transaction = Transfer(
        recipientId=tx.address,
        amount=tx.amount,
        fee=tx_fee
    )

    transaction.set_nonce(get_nonce(address_from_passphrase(pp)) + 1)
    transaction.schnorr_sign(pp)

    if sp == "None":
        sp = None
    if sp is not None:
        transaction.second_sign(sp)

    transaction_dict = transaction.to_dict()
    return transaction_dict


def build_multi_transaction(txs: [Transaction], tx_fee: int, pp: str, sp: str = None):
    transaction = MultiPayment(
        fee=tx_fee
    )
    for tx in txs:
        transaction.add_payment(tx.amount, tx.address)

    transaction.set_nonce(get_nonce(address_from_passphrase(pp)) + 1)
    transaction.schnorr_sign(pp)

    if sp == "None":
        sp = None
    if sp is not None:
        transaction.second_sign(sp)

    transaction_dict = transaction.to_dict()
    return transaction_dict


def get_nonce(address):
    try:
        n = client.wallets.get(address)
        return int(n["data"]["nonce"])
    except ArkHTTPException as e:
        if e.response.status_code == 404:
            return 0
        else:
            raise e
